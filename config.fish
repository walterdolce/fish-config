# General system settings
set -gx  LC_ALL en_GB.UTF-8

############################################ 
## Command aliases (Walter Dolce specific) #
############################################
# cd
alias cd..='cd ..'
# cp
alias cpd='cp -R'
# rm
alias rma='rm -rf'
# ls
alias lsla='ls -lAB'
alias lsv='ls -ABlFhT'
alias lsl='ls -l'
# git
alias gs='git status'
alias gaa='git add -A'
alias ga='git add'
alias gc='git checkout'
alias gr='git remote'
alias grv='git remote -v'
alias gb='git branch'
alias gbr='git branch -r'
# vagrant
alias vs='vagrant status'
alias vssh='vagrant ssh'
alias vd='vagrant destroy'
alias vp='vagrant provision'
alias vu='vagrant up'
alias vr='vagrant reload'
alias vrp='vagrant reload --provision'
alias vh='vagrant halt'
# System specific
alias edithosts='sudo vi /etc/hosts'
# Python
alias py='python'
# Ruby gems
alias csass='sass --watch --load-path="css/sass" css:css'
# Ruby - Chef - test-kitchen
alias kitchen='bin/kitchen'
alias k='bin/kitchen'
alias kcv='bin/kitchen converge; bin/kitchen verify'
alias kc='bin/kitchen converge'
alias kv='bin/kitchen verify'
alias kt='bin/kitchen test'
# VirtualBox - VBoxManage
alias vbmlr='VBoxManage list runningvms'
alias vbml='VBoxManage list vms'
# Ruby - Hobo (inviqa)
alias hvu='hobo vm up'
alias hvs='hobo vm ssh'
alias hvr='hobo vm reload'
#######################################
## Variables (Walter Dolce specific) ##
#######################################
set -x EDITOR vim
# Fish  settings
set fish_greeting ""
set fish_path $HOME/.oh-my-fish
set fish_theme bobthefish
set -g theme_display_user yes
set -g default_user your_normal_user
# Generic executables settings (PhpSpec, Behat, etc)
alias phpcs='./bin/phpcs.phar'
alias phpmd='./bin/phpmd.phar'
alias phpspec='./bin/phpspec'
alias pspec='phpspec'
alias behat='./bin/behat'
alias phpstorm='open -a PhpStorm.app .'
alias pstorm='open -a PhpStorm.app .'
alias composer='composer.phar'
alias selenium='java -jar ~/bin/selenium-server-standalone-2.43.1.jar'
# Plugins
# Which plugins would you like to load? (plugins can be found in ~/.oh-my-fish/plugins/*)
# Custom plugins may be added to ~/.oh-my-fish/custom/plugins/
# Example format: set fish_plugins autojump bundler
set fish_plugins brew bundler git php rbenv git-flow theme
 
# Load oh-my-fish configuration
. $fish_path/oh-my-fish.fish
 
set PATH ~/bin /usr/local/bin /usr/local/sbin $PATH
 
if test -e ~/.rbenv/bin/rbenv
    set PATH ~/.rbenv/bin $PATH
    . (rbenv init - | psub)
    rbenv rehash > /dev/null ^&1
end
